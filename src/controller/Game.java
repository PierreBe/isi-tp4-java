package controller;
import java.net.Socket;

import model.AdversaireReseau;
import model.IJoueur;
import model.Joueur;
import model.Ordi;
import view.MenuWindow;
import view.IGameWindow;
import view.ConnectionWindow;
import view.EndWindow;
import view.GameWindow;
import view.HelpWindow;

public class Game{
	
	private static IJoueur joueur;
	private static IJoueur adversaire;
	private static Socket socket;
	private static MenuWindow menuWindow=new MenuWindow(/*controller*/);
	private static ConnectionWindow connectionWindow;
	private static HelpWindow helpWindow=new HelpWindow(/*controller*/);
	private static IGameWindow gameWindow;
	private static EndWindow endWindow=new EndWindow(/*controller*/);
	private static IController controller;
	static IController controllerReseau;
	
	
	private static short gridHeight;
	private static short gridWidth;
	private static short nbShips;
	private static short[] shipLengths;
	private static boolean contreLOrdi;
	private static boolean afficherVaisseauxAdversaire;
	
	public static void main(String[] args){
		setDifficulty();
		startMenu();
		joueur=new Joueur(gridHeight,gridWidth,nbShips,shipLengths);
	}
	private static void setDifficulty(){
		gridHeight=10;
		gridWidth=10;
		nbShips=4;
		shipLengths=new short[]{4,3,3,2};
		afficherVaisseauxAdversaire=false;	// c'est plus marrant de le mettre � false ... (marche que contre l'ordi)
	}
	public static void startMenu(){
		menuWindow.setVisible(true);
	}
	public static void startGame(String string){
		if(string=="reseau"){
			contreLOrdi=false;
		}else{
			contreLOrdi=true;
			controllerReseau=new ControllerReseau(socket,adversaire,joueur);
			adversaire=new Ordi(gridHeight,gridWidth,nbShips,shipLengths);
		}

		gameWindow=new GameWindow(gridHeight,gridWidth);
		controller=new Controller(joueur,adversaire,contreLOrdi,gameWindow,socket,afficherVaisseauxAdversaire);
		((Controller)controller).start();
	}
	public static void connectionReseau(){
		adversaire=new AdversaireReseau();
		controllerReseau=new ControllerReseau(socket,adversaire,joueur);
		connectionWindow=new ConnectionWindow(controllerReseau);
		connectionWindow.setVisible(true);
	}
	// startServerGameplay
	// startClientGameplay 	
	public static void Rejouer(){
		if(contreLOrdi){
			startGame("ordi");
		}else{
			startGame("reseau");
		}
	}
	public static void endGame(){
		String string;
		if(joueur.hasWon()){
			string="Vous avez gagn� !";
		}else if(adversaire.hasWon()){
			string="Vous avez perdu ...";
		}else {
			string="oups ...";
		}
		endWindow.setText(string);
		gameWindow.setVisible(false);
		endWindow.setVisible(true);
	}
	public static void getHelp(){
		helpWindow.setVisible(true);
	}
}