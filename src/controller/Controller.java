package controller;

import java.net.Socket;

import model.IJoueur;
import view.IGameWindow;

public class Controller extends Thread implements IController{
	private IJoueur joueur;
	private IJoueur adversaire;
	private boolean contreLOrdi;
	private IGameWindow gameWindow;
	private Socket socket;
	private boolean afficherVaisseauxAdversaire;
	public Controller(IJoueur joueur,IJoueur adversaire,boolean contreLOrdi,IGameWindow gameWindow,Socket socket,boolean afficherVaisseauxAdversaire){
		this.joueur=joueur;
		this.adversaire=adversaire;
		this.gameWindow=gameWindow;
		this.contreLOrdi=contreLOrdi;
		this.gameWindow.setController(this);
		this.socket=socket;
		this.afficherVaisseauxAdversaire=afficherVaisseauxAdversaire;
		//this.setInfosPartie();	// bloque le d�but de partie r�seau
	}
	
	public String verifierSaisie(String iPAdress,String nPort){return "";}
	
	public void run(){
		this.gameWindow.placerBateaux();
		while(!(this.joueur.isPlacementFini()&&this.adversaire.isPlacementFini())){
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		this.gameWindow.finPlacerBateaux();

		if(this.afficherVaisseauxAdversaire){
			for(short i=0;i<this.adversaire.getGrid().getNbShips();i++){
				for(short j=0;j<this.adversaire.getGrid().getShips()[i].getLength();j++){
					this.gameWindow.printCell(true,(short)(this.adversaire.getGrid().getShips()[i].getX()+(this.adversaire.getGrid().getShips()[i].isVertical()?0:j)),(short)
							(this.adversaire.getGrid().getShips()[i].getY()+(this.adversaire.getGrid().getShips()[i].isVertical()?j:0)),"gray");
				}
			}
		}
		
		if(((ControllerReseau)Game.controllerReseau).pileOuFace()){//
			this.joueur.setShot(false);	// UN SEUL BOOL AMOIDETIRER
			this.adversaire.setShot(true);
		}else{
			this.joueur.setShot(true);
			this.adversaire.setShot(false);
		}
		
		while(!(this.joueur.hasWon()||this.adversaire.hasWon())){
			if(this.contreLOrdi)
				this.shootIA();
			//else
				//this.shootAdversaireReseau();
			this.setInfosPartie();
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		Game.endGame();
	}
	
	public short[] getShipsLengths(){
		short[] lengths=new short[this.joueur.getGrid().getShips().length];
		for(short i=0;i<this.joueur.getGrid().getShips().length;i++){
			lengths[i]=this.joueur.getGrid().getShips()[i].getLength();
		}
		return lengths;
	}
		
	public boolean placeShipsJoueur(short x1,short y1,short x2,short y2){
		short indice=this.joueur.placerBateau(x1,y1,x2,y2);
		if(indice!=-1){
			for(short i=0;i<this.joueur.getGrid().getShips()[indice].getLength();i++){
				this.gameWindow.printCell(false,(short)(this.joueur.getGrid().getShips()[indice].getX()+(this.joueur.getGrid().getShips()[indice].isVertical()?0:i)),(short)
						(this.joueur.getGrid().getShips()[indice].getY()+(this.joueur.getGrid().getShips()[indice].isVertical()?i:0)),"gray");
			}
			return true;
		}
		return false;
	}
	
	public void shootJoueur(short x,short y){
		if(!this.joueur.hasShot()&&!this.joueur.isShotAlreadyDone(x,y)){
			String string="Vous tirez en "+(char)(x+65)+(y+1)+" : ";
			if(this.adversaire.controlHit(x,y)){
				string=string+"Touch�";
				if(this.adversaire.isSunk()){
					string=string+", coul�";
				}
				this.gameWindow.printCell(true,x,y,"red");
				this.joueur.incScore();
			}else{
				string=string+"Manqu�";
				this.gameWindow.printCell(true,x,y,"white");
			}
			this.gameWindow.updateJournalPartie(string+'\n');
			this.joueur.setShot(!this.joueur.hasShot());
			this.adversaire.setShot(!this.adversaire.hasShot());
		}
	}
	
	private void shootIA(){	// lui donner une vrai intelligence et non la faire tirer au hasard
		if(!this.adversaire.hasShot()){
			short x,y;
			do{
				x=(short)(Math.random()*this.joueur.getGrid().getWidth());
				y=(short)(Math.random()*this.joueur.getGrid().getHeight());
			}while(this.adversaire.isShotAlreadyDone(x,y));
			String string="L'adversaire tire en "+(char)(x+65)+(y+1)+" : ";
			if(this.joueur.controlHit(x,y)){
				string=string+"Touch�";
				if(this.joueur.isSunk()){
					string=string+", coul�";
				}
				this.gameWindow.printCell(false,x,y,"red");
				this.adversaire.incScore();
			}else{
				string=string+"Manqu�";
				this.gameWindow.printCell(false,x,y,"white");
			}
			this.gameWindow.updateJournalPartie(string+'\n');
			this.joueur.setShot(!this.joueur.hasShot());
			this.adversaire.setShot(!this.adversaire.hasShot());
		}
	}
	
	private void setInfosPartie(){
		String string=new String("Score :\nJoueur - "+this.joueur.getScore()+"/"+this.joueur.getScoreMax()+"\nAdversaire - "+this.adversaire.getScore()+"/"+this.adversaire.getScoreMax());
		this.gameWindow.setInfosPartie(string);
	}
}