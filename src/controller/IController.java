package controller;

public interface IController{
	//public void startAIGameplay();
	public String verifierSaisie(String iPAdress,String nPort);
	public void run();
	//public void endGame();
	public short[] getShipsLengths();
	public boolean placeShipsJoueur(short x1,short y1,short x2,short y2);
	public void shootJoueur(short x,short y);
}