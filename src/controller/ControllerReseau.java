package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import model.IJoueur;

public class ControllerReseau extends Thread implements IController{
	private String iPAdress;
	private int nPort;
	private IJoueur adversaire;
	private IJoueur joueur;
	private short srv=-1;
	private Socket socket1,socket2;
	private ServerSocket serverSocket1,serverSocket2;
	private OutputStream os1,os2;
	private PrintWriter pw1,pw2;
	private InputStream is1,is2;
	private InputStreamReader isr1,isr2;
	private BufferedReader br1,br2;
	
	ControllerReseau(Socket socket,IJoueur adversaire,IJoueur joueur){
		this.socket1=socket;
		this.adversaire=adversaire;
		this.joueur=joueur;
	}
	
	public String verifierSaisie(String iPAdress,String nPort){
		if(!iPAdress.equals("localhost")){
			short bytes[]=new short[4];
			try{
				int index1=iPAdress.indexOf('.',0);
				int index2=iPAdress.indexOf('.',index1+1);
				int index3=iPAdress.indexOf('.',index2+1);
				bytes[0]=Short.valueOf(iPAdress.substring(0,index1));
				bytes[1]=Short.valueOf(iPAdress.substring(index1+1,index2));
				bytes[2]=Short.valueOf(iPAdress.substring(index2+1,index3));
				bytes[3]=Short.valueOf(iPAdress.substring(index3+1,iPAdress.length()));
			}catch(Throwable e){
				return "Adresse IP : Entrez une adresse IP valide (w.x.y.z o� w,x,y et z sont des nombres entiers).";
			}
			for(short octet:bytes){
				if((octet<0||octet>255)){
					return "Adresse IP : Une adresse IP valide est compos�e de nombres entiers compris entre 0 et 255.";
				}
			}
		}
		//int numPort;
		try{
		this.nPort=Integer.valueOf(nPort);
			//numPort=Integer.valueOf(nPort);
		}catch(Throwable e){
			return "Port : Vous devez saisir un nombre entier";
		}
		if((this.nPort<1025||this.nPort>65534)){
			return "Port : Entrez un nombre compris entre 1025 et 65534";
		}
		this.iPAdress=iPAdress;
		this.start();
		return "";
	}
	
	public void run(){
		
		if(this.socket1==null||this.socket1.isClosed()){	// tentative mode client
			System.out.println("Tentative de connexion � l'adversaire ...");
			try{
				this.socket1=new Socket(this.iPAdress,this.nPort+1);
				this.socket2=new Socket(this.iPAdress,this.nPort);
				this.srv=1;
			}catch(Throwable e){
				System.out.println("Echec");
				//e.printStackTrace();
			}
		}
		
		if(this.socket1==null||this.socket1.isClosed()){	// tentative mode serveur
			System.out.println("En attente de connexion de l'adversaire ...");
			try{
				this.serverSocket1=new ServerSocket(this.nPort);
				this.serverSocket2=new ServerSocket(this.nPort+1);
				this.socket1=serverSocket1.accept();
				this.socket2=serverSocket2.accept();
				this.srv=0;
			}catch(Throwable e){
				//e.printStackTrace();
			}
		}
		
		if(!(this.socket1==null||this.socket1.isClosed())&&!(this.socket2==null||this.socket2.isClosed())){
			System.out.println("Connexion �tablie");
			
			try{
				this.os1 = socket1.getOutputStream();
			}catch(IOException e){
				e.printStackTrace();
			}
			this.pw1 = new PrintWriter(os1, true);
			try {
				this.is1 = socket1.getInputStream();
			}catch (IOException e){
				e.printStackTrace();
			}
			this.isr1 = new InputStreamReader(is1);
			this.br1 = new BufferedReader(isr1);
			
			this.adversaire.setNetworkTools(this.socket1,this.os1,this.pw1,this.is1,this.isr1,this.br1);

			try{
				this.os2 = socket2.getOutputStream();
			}catch(IOException e){
				e.printStackTrace();
			}
			this.pw2 = new PrintWriter(os2, true);
			try {
				this.is2 = socket2.getInputStream();
			}catch (IOException e){
				e.printStackTrace();
			}
			this.isr2 = new InputStreamReader(is2);
			this.br2 = new BufferedReader(isr2);

			Game.startGame("reseau");
			
			String ligne="";
			
			// while(!endGame){
			while(true) {
				// rester � l'�coute des requ�tes
				try{
					ligne=this.br2.readLine();
					//ligne=this.br1.readLine();
				}catch(IOException e){
					e.printStackTrace();
				}
				//System.out.println("ControllerReseau - ligne lue : " + ligne);//asuppr

				if(ligne.equals("isPlacementFini()")){
					if(this.joueur.isPlacementFini()){
						this.pw2.print("yes\n");
						this.pw2.flush();
					}else{
						this.pw2.print("no\n");
						this.pw2.flush();
					}
				}else if(ligne.equals("hasWon()")){
					if(this.joueur.hasWon()){
						this.pw2.print("yes\n");
						this.pw2.flush();
					}else{
						this.pw2.print("no\n");
						this.pw2.flush();
					}
				}else if(ligne.equals("controlHit(short x,short y)")){
					int x=0,y=0;
					try{
						x=this.br2.read();
						y=this.br2.read();
					}catch(IOException e){
						e.printStackTrace();
					}
					
					if(this.joueur.controlHit((short)x,(short)y)){
						this.pw2.print("yes\n");
						this.pw2.flush();
					}else{
						this.pw2.print("no\n");
						this.pw2.flush();
					}
				}else if(ligne.equals("isSunk()")){
					if(this.joueur.isSunk()){
						this.pw2.print("yes\n");
						this.pw2.flush();
					}else{
						this.pw2.print("no\n");
						this.pw2.flush();
					}
				}else if(ligne.equals("hasShot()")){
					if(this.joueur.hasShot()){
						this.pw2.print("yes\n");
						this.pw2.flush();
					}else{
						this.pw2.print("no\n");
						this.pw2.flush();
					}
				}else if(ligne.equals("setShot(boolean shot)")){
					try{
						ligne=this.br2.readLine();
					}catch(IOException e){
						e.printStackTrace();
					}
					if(ligne.equals("yes")){
						this.joueur.setShot(true);
					}else{
						this.joueur.setShot(false);
					}
				}else if(ligne.equals("incScore()")){
					this.joueur.incScore();
				}else if(ligne.equals("getScoreMax()")){
					this.pw2.print(Short.toString(this.joueur.getScoreMax())+'\n');
					this.pw2.flush();
				}else if(ligne.equals("getScore()")){
					System.out.println("getScore() re�u");
					this.pw2.print(Short.toString(this.joueur.getScore())+'\n');
					this.pw2.flush();
				}
				
				try{
					Thread.sleep(1000);
				}catch(InterruptedException e){
					e.printStackTrace();
				}

				// rester � l'�coute des requ�tes
			}
		}
	}

	public short[] getShipsLengths(){
		return null;
	}

	public boolean placeShipsJoueur(short x1,short y1,short x2,short y2){
		return false;
	}

	public void shootJoueur(short x,short y){
		
	}
	
	boolean pileOuFace() {

		if(this.srv==1){	// srv
			if(Math.random()<0.5){
				this.pw2.print("1\n");
				this.pw2.flush();
				return true;
			}else{
				this.pw2.print("0\n");
				this.pw2.flush();
				return false;
			}
		}else if(this.srv==0){	// clt
			String string="";
			try{
				string=this.br2.readLine();
			}catch(IOException e){
				e.printStackTrace();
			}
			if(string.equals("1")){
				return false;
			}else{
				return true;
			}
		}else {	// vs. ia
			if(Math.random()<0.5){
				return true;
			}else{
				return false;
			}
		}
	}
}
