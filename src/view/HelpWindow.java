package view;

import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.Game;
import controller.IController;

public class HelpWindow extends JFrame implements ActionListener/*IMenuWindow*/{
	//static IController controller;
	private static int gap=7;
	
	public HelpWindow(/*IController controller*/){
		super("battleShip");
		//EndWindow.controller=controller;
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//GridLayout layout=new GridLayout(0,1,HelpWindow.gap,HelpWindow.gap);
		GridBagLayout layout=new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel=new JPanel(layout);
		this.add(panel);
		panel.setBackground(Color.black);
		panel.setBorder(BorderFactory.createEmptyBorder(HelpWindow.gap,HelpWindow.gap,HelpWindow.gap,HelpWindow.gap));
		
		String string=new String(""
				+ "Comme dans le jeu de bataille navale classique\n"
				+ "vous devez d'abord poser vos bateaux sur la grille du bas.\n"
				+ "Un premier clic pour d�finir la premi�re extr�mit� du bateau,\n"
				+ "un deuxi�me clic pour d�finir la deuxi�me extr�mit� du bateau.\n"
				+ "Si le bateau entre en collision avec un autre ou si il n'existe pas,\n"
				+ "alors la commande sera annul�e et vous pourrez recommencer l'op�ration.\n"
				+ "\n"
				+ "Une fois tous les bateaux plac�s vous pourrez commencer � jouer.\n"
				+ "Le joueur qui commence est determin� au hasard.\n"
				+ "Cliquez sur une case de la grille du haut pour lancer une torpille.\n"
				+ "Si la case devient rouge, vous avez touch� l'ennemi.\n"
				+ "Si la case devient blanche, vous avez manqu� l'ennemi.\n"
				+ "\n"
				+ "Vous pouvez � tout moment consulter le score ainsi qu'acc�der\n"
				+ "au journal de la partie sur la partie droite de l'�cran.\n"
				+ "\n"
				+ "Bon jeu !");
		
		c.fill=GridBagConstraints.HORIZONTAL;
		c.gridwidth=3;
		JTextArea text=new JTextArea(string);
		text.setBackground(Color.black);
		text.setForeground(Color.white);
		text.setEditable(false);
		panel.add(text,c);
		
		c.weightx=1;
		c.gridwidth=1;
		c.gridx=0;
		c.gridy=1;
		JLabel label=new JLabel();
		label.setBackground(Color.black);
		panel.add(label,c);
		c.gridx=2;
		label=new JLabel();
		label.setBackground(Color.black);
		panel.add(label,c);
		
		c.gridwidth=1;
		c.gridx=1;
		c.ipady=HelpWindow.gap;
		c.insets = new Insets(HelpWindow.gap,0,0,0);
		Button boutonMenu=new Button("Menu");
		boutonMenu.setBackground(Color.black);
		boutonMenu.setForeground(Color.white);
		boutonMenu.addActionListener(this);
		panel.add(boutonMenu,c);

		c.gridy=2;
		Button boutonQuitter=new Button("Quitter");
		boutonQuitter.setBackground(Color.black);
		boutonQuitter.setForeground(Color.white);
		boutonQuitter.addActionListener(this);
		panel.add(boutonQuitter,c);

		this.pack();
	}
	public void actionPerformed(ActionEvent e) {
		switch(((Button)e.getSource()).getLabel()){
			case "Menu":
				Game.startMenu();
				break;
			case "Quitter":
				System.exit(EXIT_ON_CLOSE);
				break;
		}
		this.setVisible(false);
	}
}