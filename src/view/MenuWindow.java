package view;

import java.awt.Button;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import controller.Game;
import controller.IController;

public class MenuWindow extends JFrame implements ActionListener/*IMenuWindow*/{
	//static IController controller;
	private static int gap=7;
	
	public MenuWindow(/*IController controller*/){
		super("battleShip");
		//MenuWindow.controller=controller;
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		GridLayout layout=new GridLayout(0,1,MenuWindow.gap,MenuWindow.gap);
		JPanel panel=new JPanel(layout);
		this.add(panel);
		panel.setBackground(Color.black);
		panel.setBorder(BorderFactory.createEmptyBorder(MenuWindow.gap,MenuWindow.gap,MenuWindow.gap,MenuWindow.gap));
		
		Label titre=new Label("battleShip",Label.CENTER);
		titre.setBackground(Color.black);
		titre.setForeground(Color.white);
		panel.add(titre);
		
		Button boutonJeuOrdi=new Button("Jouer contre l'ordinateur");
		boutonJeuOrdi.setBackground(Color.black);
		boutonJeuOrdi.setForeground(Color.white);
		boutonJeuOrdi.addActionListener(this);
		panel.add(boutonJeuOrdi);
		
		Button boutonJeuReseau=new Button("Jouer en r�seau");
		boutonJeuReseau.setBackground(Color.black);
		boutonJeuReseau.setForeground(Color.white);
		boutonJeuReseau.addActionListener(this);
		panel.add(boutonJeuReseau);
		
		Button boutonCommentJouer=new Button("Comment jouer");
		boutonCommentJouer.setBackground(Color.black);
		boutonCommentJouer.setForeground(Color.white);
		boutonCommentJouer.addActionListener(this);
		panel.add(boutonCommentJouer);
		
		Button boutonQuitter=new Button("Quitter");
		boutonQuitter.setBackground(Color.black);
		boutonQuitter.setForeground(Color.white);
		boutonQuitter.addActionListener(this);
		panel.add(boutonQuitter);
		
		JTextArea footer=new JTextArea("Raphael Saint-Louis\nPierre Bodineau");
		footer.setBackground(Color.black);
		footer.setForeground(Color.gray);
		footer.setEditable(false);
		panel.add(footer);

		this.pack();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(((Button)e.getSource()).getLabel()){
			case "Jouer contre l'ordinateur":
				Game.startGame("ordi");
				break;
			case "Jouer en r�seau":
				Game.connectionReseau();
				break;
			case "Comment jouer":
				Game.getHelp();
				break;
			case "Quitter":
				System.exit(EXIT_ON_CLOSE);
				break;
		}
		this.setVisible(false);
	}
}