package view;

import java.awt.Button;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import controller.Game;
import controller.IController;

public class EndWindow extends JFrame implements ActionListener/*IMenuWindow*/{
	//static IController controller;
	private JTextArea text;
	private static int gap=7;
	
	public EndWindow(/*IController controller*/){
		super("battleShip");
		//EndWindow.controller=controller;
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		GridLayout layout=new GridLayout(0,1,EndWindow.gap,EndWindow.gap);
		JPanel panel=new JPanel(layout);
		this.add(panel);
		panel.setBackground(Color.black);
		panel.setBorder(BorderFactory.createEmptyBorder(EndWindow.gap,EndWindow.gap,EndWindow.gap,EndWindow.gap));
		
		text=new JTextArea();
		text.setBackground(Color.black);
		text.setForeground(Color.white);
		text.setEditable(false);
		panel.add(text);
		
		Button boutonRejouer=new Button("Rejouer");
		boutonRejouer.setBackground(Color.black);
		boutonRejouer.setForeground(Color.white);
		boutonRejouer.addActionListener(this);
		panel.add(boutonRejouer);
		
		Button boutonMenu=new Button("Menu");
		boutonMenu.setBackground(Color.black);
		boutonMenu.setForeground(Color.white);
		boutonMenu.addActionListener(this);
		panel.add(boutonMenu);
		
		Button boutonQuitter=new Button("Quitter");
		boutonQuitter.setBackground(Color.black);
		boutonQuitter.setForeground(Color.white);
		boutonQuitter.addActionListener(this);
		panel.add(boutonQuitter);

		this.pack();
	}
	public void setText(String string){
		this.text.setText(string);
	}
	public void actionPerformed(ActionEvent e) {
		switch(((Button)e.getSource()).getLabel()){
			case "Rejouer":
				Game.Rejouer();
				break;
			case "Menu":
				Game.startMenu();
				break;
			case "Quitter":
				System.exit(EXIT_ON_CLOSE);
				break;
		}
		this.setVisible(false);
	}
}