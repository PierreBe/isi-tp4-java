package view;

import java.awt.Button;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.Game;
import controller.IController;

public class ConnectionWindow extends JFrame implements ActionListener/*IMenuWindow*/{
	static IController controller;
	private JTextField fieldIP;
	private JTextField fieldPort;
	private static int gap=7;
	
	JLabel label;//
	
	public ConnectionWindow(IController controller){
		super("battleShip");
		ConnectionWindow.controller=controller;
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		GridLayout layout=new GridLayout(0,1,ConnectionWindow.gap,ConnectionWindow.gap);
		JPanel panel=new JPanel(layout);
		this.add(panel);
		panel.setBackground(Color.black);
		panel.setBorder(BorderFactory.createEmptyBorder(ConnectionWindow.gap,ConnectionWindow.gap,ConnectionWindow.gap,ConnectionWindow.gap));
		
		JTextArea labelIP=new JTextArea("\nAdresse IP de l'adversaire :");
		labelIP.setBackground(Color.black);
		labelIP.setForeground(Color.white);
		labelIP.setEditable(false);
		panel.add(labelIP);

		fieldIP=new JTextField();
		fieldIP.setBackground(Color.black);
		fieldIP.setForeground(Color.white);
		fieldIP.setCaretColor(Color.white);
		panel.add(fieldIP);

		JTextArea labelPort=new JTextArea("\nPort de connexion :");
		labelPort.setBackground(Color.black);
		labelPort.setForeground(Color.white);
		labelPort.setEditable(false);
		panel.add(labelPort);

		fieldPort=new JTextField();
		fieldPort.setBackground(Color.black);
		fieldPort.setForeground(Color.white);
		fieldPort.setCaretColor(Color.white);
		panel.add(fieldPort);
		
		Button boutonConnecter=new Button("Connecter");
		boutonConnecter.setBackground(Color.black);
		boutonConnecter.setForeground(Color.white);
		boutonConnecter.addActionListener(this);
		panel.add(boutonConnecter);
		
		/*JLabel */label=new JLabel();
		label.setBackground(Color.black);
		label.setForeground(Color.white);//
		panel.add(label);
		
		Button boutonMenu=new Button("Menu");
		boutonMenu.setBackground(Color.black);
		boutonMenu.setForeground(Color.white);
		boutonMenu.addActionListener(this);
		panel.add(boutonMenu);
		
		Button boutonQuitter=new Button("Quitter");
		boutonQuitter.setBackground(Color.black);
		boutonQuitter.setForeground(Color.white);
		boutonQuitter.addActionListener(this);
		panel.add(boutonQuitter);

		this.pack();
	}
	public void actionPerformed(ActionEvent e) {
		switch(((Button)e.getSource()).getLabel()){
			case "Connecter":
				String string=ConnectionWindow.controller.verifierSaisie(this.fieldIP.getText(),this.fieldPort.getText());
				if(string!=""){
					label.setText(string);
					this.pack();
				}else{
					this.setVisible(false);
				}
				break;
			case "Menu":
				Game.startMenu();
				this.setVisible(false);
				break;
			case "Quitter":
				System.exit(EXIT_ON_CLOSE);
				break;
		}
	}
}