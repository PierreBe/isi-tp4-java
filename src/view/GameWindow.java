package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import controller.IController;

public class GameWindow extends JFrame implements IGameWindow{
	private JTextArea northEastTextArea;
	private JTextArea southEastTextArea;
	private Grid topGrid;
	private Grid bottomGrid;
	static IController controller;
	public GameWindow(short gridHeight,short gridWidth){
		super("battleShip");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		/*JTextArea northTextArea=new JTextArea("Infos Jeu");
		northTextArea.setBackground(Color.black);
		northTextArea.setForeground(Color.white);
		northTextArea.setEditable(false);
		this.add(northTextArea,BorderLayout.NORTH);*/

		/*JTextArea westTextArea=new JTextArea("Menu");
		westTextArea.setBackground(Color.black);
		westTextArea.setForeground(Color.white);
		westTextArea.setEditable(false);
		this.add(westTextArea,BorderLayout.WEST);*/

		JPanel eastBoard=new JPanel();
		eastBoard.setLayout(new GridLayout(2,1));
		this.northEastTextArea=new JTextArea(0,12);
		this.northEastTextArea.setBackground(Color.black);
		this.northEastTextArea.setForeground(Color.white);
		this.northEastTextArea.setEditable(false);
		eastBoard.add(this.northEastTextArea,BorderLayout.EAST);
		this.southEastTextArea=new JTextArea("D�but partie\n\n",0,20);
		this.southEastTextArea.setBackground(Color.black);
		this.southEastTextArea.setForeground(Color.white);
		this.southEastTextArea.setEditable(false);
		eastBoard.add(new JScrollPane(this.southEastTextArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
		this.add(eastBoard,BorderLayout.EAST);
		
		/*JTextArea southTextArea=new JTextArea("Infos Application");
		southTextArea.setBackground(Color.black);
		southTextArea.setForeground(Color.white);
		southTextArea.setEditable(false);
		this.add(southTextArea,BorderLayout.SOUTH);*/

		JPanel board=new JPanel();
		board.setLayout(new GridLayout(2,1));
		this.topGrid=new Grid(gridHeight,gridWidth,true);
		board.add(this.topGrid);
		this.bottomGrid=new Grid(gridHeight,gridWidth,false);
		board.add(this.bottomGrid);
		this.add(board,BorderLayout.CENTER);

		this.pack();
		this.setVisible(true);
	}
	public void setController(IController controller){
		GameWindow.controller=controller;
	}
	public void setInfosPartie(String string){
		this.northEastTextArea.setText(string);
	}
	public void updateJournalPartie(String string){
		this.southEastTextArea.append(string);
		this.southEastTextArea.setCaretPosition(this.southEastTextArea.getDocument().getLength());
	}
	public void printCell(boolean topGrid,short x,short y,String colorName){
		if(topGrid){
			this.topGrid.printCell(x,y,colorName);
		}else {
			this.bottomGrid.printCell(x,y,colorName);
		}
	}
	public void placerBateaux(){
		this.bottomGrid.addMouseListener(false);
	}
	public void finPlacerBateaux(){
		// si la case actuelle est bleu il faut la remettre en noir
		if(BottomGridListener.hovered.getBackground()==Color.blue)
			BottomGridListener.hovered.setBackground(Color.black);
		
		this.bottomGrid.removeMouseListener();
		this.topGrid.addMouseListener(true);
	}
}

class Grid extends JPanel{
	private short gridHeight;
	private short gridWidth;
	private Case[][] squares;

	Grid(short gridHeight,short gridWidth,boolean top){
		this.gridHeight=gridHeight;
		this.gridWidth=gridWidth;
		this.squares=new Case[this.gridHeight][this.gridWidth];
		this.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		this.setLayout(new GridLayout(this.gridHeight,this.gridWidth));
		this.setBackground(Color.black);

		for(short i=0;i<this.gridHeight;i++){
			for(short j=0;j<this.gridWidth;j++){
				this.squares[i][j]=new Case(j,i);
				this.add(squares[i][j]);
			}
		}
	}
	void addMouseListener(boolean top){
		if(top){
			for(short i=0;i<this.gridHeight;i++){
				for(short j=0;j<this.gridWidth;j++){
					this.squares[i][j].addMouseListener(new TopGridListener());
				}
			}
		}else{
			BottomGridListener.shipsLengths=GameWindow.controller.getShipsLengths();
			BottomGridListener.selection=new Case[BottomGridListener.shipsLengths.length];
			BottomGridListener.indiceSelection=0;
			for(short i=0;i<this.gridHeight;i++){
				for(short j=0;j<this.gridWidth;j++){
					this.squares[i][j].addMouseListener(new BottomGridListener());
				}
			}
		}
	}
	void removeMouseListener(){
		for(short i=0;i<this.gridHeight;i++){
			for(short j=0;j<this.gridWidth;j++){
				this.squares[i][j].removeMouseListener(this.squares[i][j].getMouseListeners()[0]);
			}
		}
	}
	
	void printCell(short x,short y,String colorName){
		Color color=Color.black;
		switch(colorName){
			case "gray":color=Color.gray;break;
			case "red":color=Color.red;break;
			case "white":color=Color.white;break;
		}
		this.squares[y][x].setBackground(color);
		this.squares[y][x].setForeground(color);
	}
	
	Case[][] getSquares(){
		return this.squares;
	}
}

class Case extends JLabel{
	private short x,y;
	Case(short x,short y){
		this.x=x;
		this.y=y;
		this.setOpaque(true);
		this.setPreferredSize(new Dimension(30,30));
		this.setBackground(Color.black);
		this.setBorder(BorderFactory.createLineBorder(Color.darkGray,1));
	}
	short getPosX(){
		return this.x;
	}
	short getPosY(){
		return this.y;
	}
}

class TopGridListener implements MouseListener{
	private boolean in;
	public void mouseEntered(MouseEvent e){
		this.in=true;
		if(((JLabel)e.getSource()).getBackground()==Color.black){
			((JLabel)e.getSource()).setBackground(Color.blue);
		}
	}
	public void mouseExited(MouseEvent e){
		this.in=false;
		if(((JLabel)e.getSource()).getBackground()==Color.blue||((JLabel)e.getSource()).getBackground()==Color.green){
			((JLabel)e.getSource()).setBackground(Color.black);
		}
	}
	
	public void mousePressed(MouseEvent e){
		if(((JLabel)e.getSource()).getBackground()==Color.blue){
			((JLabel)e.getSource()).setBackground(Color.green);
		}
	}
	public void mouseReleased(MouseEvent e){
		if(this.in){
			this.mouseClicked(e);
		}
	}
	public void mouseClicked(MouseEvent e){
		GameWindow.controller.shootJoueur(((Case)e.getSource()).getPosX(),((Case)e.getSource()).getPosY());
	}
}

class BottomGridListener implements MouseListener{
	static short[] shipsLengths;
	static Case selection[];
	static Case hovered;
	static short indiceSelection;
	private boolean in;
	
	public void mouseEntered(MouseEvent e){

		this.in=true;
		if(BottomGridListener.selection[0]==null){	// si on n'a pas encore cliqu�
			if(((JLabel)e.getSource()).getBackground()==Color.black){
				((JLabel)e.getSource()).setBackground(Color.blue);
				
				BottomGridListener.hovered=((Case)e.getSource());
				
			}
		}else{	// si on a d�j� cliqu�
			for(short i=1;i<=BottomGridListener.indiceSelection;i++){
				if(BottomGridListener.selection[i].getBackground()==Color.blue)BottomGridListener.selection[i].setBackground(Color.black);
				BottomGridListener.selection[i]=null;
			}
			short longestShip=0,length=0;
			for(short i=0;i<BottomGridListener.shipsLengths.length;i++){
				if(BottomGridListener.shipsLengths[i]>longestShip)longestShip=(short)(BottomGridListener.shipsLengths[i]-1);
			}
			if(((Case)e.getSource()).getPosX()==BottomGridListener.selection[0].getPosX()){ // si on est aligne en x avec le premier clic
				length=(short)(((Case)e.getSource()).getPosY()-BottomGridListener.selection[0].getPosY());
				if(length<0){	// si on est � gauche de l'origine
					if(length<-longestShip)length=(short)-longestShip;
					BottomGridListener.indiceSelection=(short)-length;
					for(short i=1;i<=BottomGridListener.indiceSelection;i++){
						BottomGridListener.selection[i]=((Grid)((Case)e.getSource()).getParent()).getSquares()[BottomGridListener.selection[0].getPosY()-i][BottomGridListener.selection[0].getPosX()];
						if(BottomGridListener.selection[i].getBackground()==Color.black)BottomGridListener.selection[i].setBackground(Color.blue);
					}
				}else{	// si on est � droite de l'origine
					if(length>longestShip)length=longestShip;
					BottomGridListener.indiceSelection=length;
					for(short i=1;i<=BottomGridListener.indiceSelection;i++){
						BottomGridListener.selection[i]=((Grid)((Case)e.getSource()).getParent()).getSquares()[BottomGridListener.selection[0].getPosY()+i][BottomGridListener.selection[0].getPosX()];
						if(BottomGridListener.selection[i].getBackground()==Color.black)BottomGridListener.selection[i].setBackground(Color.blue);
					}
				}

			}else if(((Case)e.getSource()).getPosY()==BottomGridListener.selection[0].getPosY()){ // si on est aligne en y avec le premier clic
				length=(short)(((Case)e.getSource()).getPosX()-BottomGridListener.selection[0].getPosX());
				if(length<0){	// si on est au dessus de l'origine
					if(length<-longestShip)length=(short)-longestShip;
					BottomGridListener.indiceSelection=(short)-length;
					for(short i=1;i<=BottomGridListener.indiceSelection;i++){
						BottomGridListener.selection[i]=((Grid)((Case)e.getSource()).getParent()).getSquares()[BottomGridListener.selection[0].getPosY()][BottomGridListener.selection[0].getPosX()-i];
						if(BottomGridListener.selection[i].getBackground()==Color.black)BottomGridListener.selection[i].setBackground(Color.blue);
					}
				}else{	// si on est en dessous de l'origine
					if(length>longestShip)length=longestShip;
					BottomGridListener.indiceSelection=length;
					for(short i=1;i<=BottomGridListener.indiceSelection;i++){
						BottomGridListener.selection[i]=((Grid)((Case)e.getSource()).getParent()).getSquares()[BottomGridListener.selection[0].getPosY()][BottomGridListener.selection[0].getPosX()+i];
						if(BottomGridListener.selection[i].getBackground()==Color.black)BottomGridListener.selection[i].setBackground(Color.blue);
					}
				}
				
			}else{	// si on n'est pas align�
				BottomGridListener.indiceSelection=0;
			}
		}
	}
	public void mouseExited(MouseEvent e){
		this.in=false;
		if(BottomGridListener.selection[0]==null){	// si on n'a pas encore cliqu�
			if(((JLabel)e.getSource()).getBackground()==Color.blue){
				((JLabel)e.getSource()).setBackground(Color.black);
			}
		}
	}
	
	public void mousePressed(MouseEvent e){
		
	}
	public void mouseReleased(MouseEvent e){
		if(this.in){
			if(((JLabel)e.getSource()).getBackground()==Color.blue&&BottomGridListener.selection[0]==null){	// si premier clic
				((JLabel)e.getSource()).setBackground(Color.green);
				BottomGridListener.selection[0]=((Case)e.getSource());
			}else{	// si deuxieme clic
				if(GameWindow.controller.placeShipsJoueur(BottomGridListener.selection[0].getPosX(),BottomGridListener.selection[0].getPosY(),((Case)e.getSource()).getPosX(),((Case)e.getSource()).getPosY())){	// si �a rentre
					short lengths[]=new short[4];
					lengths[0]=(short)(BottomGridListener.selection[0].getPosX()-((Case)e.getSource()).getPosX()+1);
					lengths[1]=(short)(BottomGridListener.selection[0].getPosY()-((Case)e.getSource()).getPosY()+1);
					lengths[2]=(short)(((Case)e.getSource()).getPosX()-BottomGridListener.selection[0].getPosX()+1);
					lengths[3]=(short)(((Case)e.getSource()).getPosY()-BottomGridListener.selection[0].getPosY()+1);
					for(short i=1;i<4;i++){
						if(lengths[i]>lengths[0])lengths[0]=lengths[i];
					}
					short i=0;
					while(/*i<BottomGridListener.shipsLengths.length&&*/BottomGridListener.shipsLengths[i]!=lengths[0]){i++;}
					BottomGridListener.shipsLengths[i]=0;

				}else{	// si �a ne rentre pas
					for(short i=0;i<=BottomGridListener.indiceSelection;i++){
						if(BottomGridListener.selection[i].getBackground()!=Color.gray)BottomGridListener.selection[i].setBackground(Color.black);
					}
				}
				BottomGridListener.selection[0]=null;
				BottomGridListener.indiceSelection=0;
			}
		}
	}
	public void mouseClicked(MouseEvent e){
		
	}
}