package view;

import controller.IController;

public interface IGameWindow{
	public void setController(IController controller);
	public void setInfosPartie(String string);
	public void updateJournalPartie(String string);
	public void printCell(boolean topGrid,short x,short y,String colorName);
	public void placerBateaux();
	public void finPlacerBateaux();
	
	public void setVisible(boolean b);
}