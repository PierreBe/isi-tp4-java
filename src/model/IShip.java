package model;

public interface IShip{
	public boolean isPlaced();
	public void setCoordinates(short x,short y,boolean vertical);
	public short getX();
	public short getY();
	public short getLength();
	public boolean isVertical();
	public boolean isHit(short x,short y);
	public boolean isSunk();
}