package model;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class Ordi implements IJoueur{
	private IGrid grid;
	private boolean shotsDone[][];
	private short score;
	private short scoreMax;
	private boolean shot;
	private boolean won=false;
	public Ordi(short height,short width,short nbShips,short[] lengths){
		this.grid=new Grid(height,width,nbShips,lengths);
		this.shotsDone=new boolean[width][height];
		for(short i=0;i<nbShips;i++){
			this.scoreMax+=lengths[i];
		}
	}
	public short getScore(){
		return this.score;
	}
	public short getScoreMax(){
		return this.scoreMax;
	}
	public IGrid getGrid(){
		return this.grid;
	}
	public boolean isShotAlreadyDone(short x,short y){
		if(this.shotsDone[x][y])
			return true;
		this.shotsDone[x][y]=true;
		return false;
	}
	public void incScore(){
		this.score++;
	}
	public boolean hasWon(){
		if(this.score==this.scoreMax)this.won=true;
		return this.won;
	}
	public short placerBateau(short x1,short y1,short x2, short y2){
		short x,y;
		boolean vertical,collision;
		for(short i=0;i<this.grid.getNbShips();i++){
			
			do{
				collision=false;
				vertical=(Math.random()<0.5?false:true);
				if(vertical){
					x=((Double)(Math.random()*this.grid.getWidth())).shortValue();
					y=((Double)(Math.random()*(this.grid.getHeight()-this.grid.getShips()[i].getLength()))).shortValue();
				}else{
					x=((Double)(Math.random()*(this.grid.getWidth()-this.grid.getShips()[i].getLength()))).shortValue();
					y=((Double)(Math.random()*this.grid.getHeight())).shortValue();
				}

				for(short j=0;j<i;j++){
					for(short k=0;k<this.grid.getShips()[j].getLength();k++){
						for(short l=0;l<this.grid.getShips()[i].getLength();l++){
							if(this.grid.getShips()[j].getX()+(this.grid.getShips()[j].isVertical()?0:k)==x+(vertical?0:l)&&this.grid.getShips()[j].getY()+(this.grid.getShips()[j].isVertical()?k:0)==y+(vertical?l:0)){
								collision=true;
								l=this.grid.getShips()[i].getLength();
								k=this.grid.getShips()[j].getLength();
								j=i;
							}
						}
					}
				}
			}while(collision);
			
			this.grid.placeShip(i,x,y,vertical);	
		}
		return 0;
	}
	public boolean isPlacementFini(){
		this.placerBateau((short)0,(short)0,(short)0,(short)0);
		return true;
	}
	
	public void setShot(boolean shot){
		this.shot=shot;
	}
	public boolean hasShot(){
		return this.shot;
	}
	
	public void shoot(){	// lui donner une vrai intelligence et non la faire tirer au hasard	// doit partir dans la classe Ordi

		if(!this.hasShot()){
			short x,y;
			do{
				x=(short)(Math.random()*this.grid.getWidth());
				y=(short)(Math.random()*this.grid.getHeight());
			}while(this.isShotAlreadyDone(x,y));
		}
	}
	public void setNetworkTools(Socket socket,OutputStream os,PrintWriter pw,InputStream is,InputStreamReader isr,BufferedReader br) {}
	public boolean controlHit(short x,short y){
		return this.grid.controlHit(x, y);
	}
	public boolean isSunk(){
		return this.grid.isSunk();
	}
}