package model;

public class Ship implements IShip{
	private boolean placed;
	private short x;
	private short y;
	private boolean vertical;
	private short length;
	private boolean[] hit;
	private boolean sunk;

	public Ship(short length){
		this.length=length;
		this.hit=new boolean[this.length];
	}
	
	public boolean isPlaced(){
		return this.placed;
	}

	public void setCoordinates(short x,short y,boolean vertical){
		this.x=x;
		this.y=y;
		this.vertical=vertical;
		this.placed=true;
	}

	public short getX(){
		return this.x;
	}
	public short getY(){
		return this.y;
	}
	public short getLength(){
		return this.length;
	}
	public boolean isVertical(){
		return this.vertical;
	}

	public boolean isHit(short x,short y){
		if(!this.sunk){
			if(this.vertical&&this.x==x){
				for(short i=0;i<this.length;i++){
					if(this.y+i==y){
						this.hit[i]=true;
						return true;
					}
				}
			}else if(!this.vertical&&this.y==y){
				for(short i=0;i<this.length;i++){
					if(this.x+i==x){
						this.hit[i]=true;
						return true;
					}
				}
			}
		}
		return false;
	}
	public boolean isSunk(){
		if(!this.sunk){
			short i=0;
			while(i<this.length&&this.hit[i]==true){i++;}
			if(i==this.length){this.sunk=true;}
		}
		return this.sunk;
	}
}