package model;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public interface IJoueur{
	public short getScore();
	public short getScoreMax();
	public IGrid getGrid();
	public boolean isShotAlreadyDone(short x,short y);
	public void incScore();
	public boolean hasWon();
	public short placerBateau(short x1,short y1,short x2, short y2);
	public boolean isPlacementFini();
	public void setShot(boolean shot);
	public boolean hasShot();
	public void shoot();
	public void setNetworkTools(Socket socket,OutputStream os,PrintWriter pw,InputStream is,InputStreamReader isr,BufferedReader br);
	public boolean controlHit(short x,short y);
	public boolean isSunk();
}
