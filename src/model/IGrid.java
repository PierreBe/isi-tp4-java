package model;

public interface IGrid{
	public short getHeight();
	public short getWidth();
	public short getNbShips();
	public IShip[] getShips();
	public boolean controlHit(short x,short y);
	public void placeShip(short n,short x,short y,boolean vertical);
	public boolean isSunk();
}