package model;

public class Grid implements IGrid{
	private boolean sunk;
	private short height;
	private short width;
	private short nbShips;
	private IShip[] ships;
	public Grid(short height,short width,short nbShips,short[] lengths){
		this.height=height;
		this.width=width;
		this.nbShips=nbShips;
		this.ships=new Ship[this.nbShips];
		for(short i=0;i<this.nbShips;i++){
			this.ships[i]=new Ship(lengths[i]);
		}
	}
	public short getHeight(){
		return this.height;
	}
	public short getWidth(){
		return this.width;
	}
	public short getNbShips(){
		return this.nbShips;
	}
	public IShip[] getShips(){
		return this.ships;
	}
	public boolean controlHit(short x,short y){
		short i=0;
		boolean touche=false;
		while(i<this.nbShips&&!touche){
			if(this.ships[i].isHit(x,y)){
				touche=true;
			}else{
				i++;
			}
		}
		if(touche&&this.ships[i].isSunk()){
			this.sunk=true;
		}
		return touche;
	}
	public void placeShip(short n,short x,short y,boolean vertical){
		this.ships[n].setCoordinates(x, y, vertical);
	}
	public boolean isSunk() {
		if(this.sunk){
			this.sunk=false;
			return true;
		}
		return false;
	}
}