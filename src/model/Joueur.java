package model;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class Joueur implements IJoueur{
	private IGrid grid;
	private boolean shotsDone[][];
	private short score;
	private short scoreMax;
	private boolean shot;
	private boolean won;
	public Joueur(short height,short width,short nbShips,short[] lengths){
		this.grid=new Grid(height,width,nbShips,lengths);
		this.shotsDone=new boolean[width][height];
		for(short i=0;i<nbShips;i++){
			this.scoreMax+=lengths[i];
		}
	}
	public short getScore(){
		return this.score;
	}
	public short getScoreMax(){
		return this.scoreMax;
	}
	public IGrid getGrid(){
		return this.grid;
	}
	public boolean isShotAlreadyDone(short x,short y){
		if(this.shotsDone[x][y])
			return true;
		this.shotsDone[x][y]=true;
		return false;
	}
	public void incScore(){
		this.score++;
	}
	public boolean hasWon(){
		if(this.score==this.scoreMax)this.won=true;
		return this.won;
	}
	public short placerBateau(short x1,short y1,short x2, short y2){
		short x,y,length;
		boolean vertical;
		
		// test le bateau est orthonorm�
		if(x1==x2){
			x=x1;
			y=y1<y2?y1:y2;
			length=(short)(y2-y1>0?y2-y1:y1-y2);
			vertical=true;
		}else if(y1==y2){
			y=y1;
			x=x1<x2?x1:x2;
			length=(short)(x2-x1>0?x2-x1:x1-x2);
			vertical=false;
		}else{
			return -1;
		}
		length++;
		
		// test collision
		for(short i=0;i<this.grid.getNbShips();i++){
			if(this.grid.getShips()[i].isPlaced()){
				for(short j=0;j<length;j++){
					for(short k=0;k<this.getGrid().getShips()[i].getLength();k++){
						if(this.getGrid().getShips()[i].getX()+(this.getGrid().getShips()[i].isVertical()?0:k)==x+(vertical?0:j)&&this.getGrid().getShips()[i].getY()+(this.getGrid().getShips()[i].isVertical()?k:0)==y+(vertical?j:0)){
							return -1;
						}
					}
				}
			}
		}
		
		// test �a rentre quelque part
		for(short i=0;i<this.grid.getNbShips();i++){
			if(!this.grid.getShips()[i].isPlaced()&&this.grid.getShips()[i].getLength()==length){
				this.getGrid().placeShip(i,x,y,vertical);
				return i;
			}
		}
		
		return -1;
	}
	public boolean isPlacementFini(){
		for(short i=0;i<this.grid.getNbShips();i++){
			if(!this.grid.getShips()[i].isPlaced()){
				return false;
			}
		}
		return true;
	}
	
	public void setShot(boolean shot){
		this.shot=shot;
	}
	public boolean hasShot(){
		return this.shot;
	}
	
	public void shoot(){}
	public void setNetworkTools(Socket socket,OutputStream os,PrintWriter pw,InputStream is,InputStreamReader isr,BufferedReader br) {}
	public boolean controlHit(short x,short y){
		return this.grid.controlHit(x, y);
	}
	public boolean isSunk(){
		return this.grid.isSunk();
	}
}