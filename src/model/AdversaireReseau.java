package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class AdversaireReseau implements IJoueur{
	
	private IGrid grid;
	/*private boolean shotsDone[][];
	private short score;
	private short scoreMax;
	private boolean shot;
	private boolean won;*/
	private Socket socket;
	private OutputStream os;
	private PrintWriter pw;
	private InputStream is;
	private InputStreamReader isr;
	private BufferedReader br;
	
	public short getScore(){
		String string="";

		this.pw.print("getScore()\n");
		this.pw.flush();

		try{
			string=this.br.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return Short.valueOf(string);
	}
	public short getScoreMax(){
		String string="";

		this.pw.print("getScoreMax()\n");
		this.pw.flush();

		try{
			string=this.br.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return Short.valueOf(string);
	}
	public IGrid getGrid(){
		return this.grid;
	}
	public boolean isShotAlreadyDone(short x,short y){
		/*if(this.shotsDone[x][y])
			return true;
		this.shotsDone[x][y]=true;*/
		return false;
	}
	public void incScore(){
		this.pw.print("incScore()\n");
	}
	public boolean hasWon(){
		String ligne="";

		this.pw.print("hasWon()\n");
		this.pw.flush();

		try{
			ligne=this.br.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		if(ligne.equals("yes"))
			return true;
		else
			return false;
	}
	public short placerBateau(short x1,short y1,short x2, short y2){
		return -1;
	}
	public boolean isPlacementFini(){
		String ligne="";

		this.pw.print("isPlacementFini()\n");
		this.pw.flush();

		try{
			ligne=this.br.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		if(ligne.equals("yes"))
			return true;
		else
			return false;
	}
	
	public void setShot(boolean shot){
		this.pw.print("setShot(boolean shot)\n");
		if(shot)
			this.pw.print("yes\n");
		else
			this.pw.print("no\n");
		this.pw.flush();
	}
	public boolean hasShot(){
		String ligne="";

		this.pw.print("hasShot()\n");
		this.pw.flush();

		try{
			ligne=this.br.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		if(ligne.equals("yes"))
			return true;
		else
			return false;
	}
	
	public void shoot(){}
	
	public void setNetworkTools(Socket socket,OutputStream os,PrintWriter pw,InputStream is,InputStreamReader isr,BufferedReader br){
		this.socket=socket;
		this.os = os;
		this.pw = pw;
		this.is = is;
		this.isr = isr;
		this.br = br;
	}
	public boolean controlHit(short x,short y){
		String ligne="";

		this.pw.print("controlHit(short x,short y)\n");
		this.pw.print((int)x);
		this.pw.print((int)y);
		this.pw.flush();

		try{
			ligne=this.br.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		if(ligne.equals("yes"))
			return true;
		else
			return false;
	}
	public boolean isSunk(){
		String ligne="";

		this.pw.print("isSunk()\n");
		this.pw.flush();

		try{
			ligne=this.br.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		if(ligne.equals("yes"))
			return true;
		else
			return false;
	}
}